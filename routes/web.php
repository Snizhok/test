<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/{category?}', [
    'as' => 'tasks.index',
    'uses' => 'TaskController@index'
])->where('category', '[0-9]+');

Route::group(['middleware' => ['web']], function () {
    Route::post('task/store', [
        'as' => 'task.store',
        'uses' => 'TaskController@store'
    ]);

    Route::delete('task/{id}', [
        'as' => 'task.destroy',
        'uses' => 'TaskController@destroy'
    ])->where('id', '[0-9]+');

    Route::post('category/store', [
        'as' => 'category.store',
        'uses' => 'CategoryController@store'
    ]);
});


