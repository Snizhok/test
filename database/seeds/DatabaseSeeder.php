<?php

use App\Models\Category;
use App\Models\Task;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(TaskSeeder::class);
    }
}

class CategorySeeder extends Seeder
{
    public function run()
    {
        DB::table('categories')->delete();
        Category::create([
            'title' => 'Category 1',
        ]);
        Category::create([
            'title' => 'Category 2',
        ]);
        Category::create([
            'title' => 'Category 3',
        ]);

    }
}

class TaskSeeder extends Seeder
{
    public function run()
    {
        DB::table('tasks')->delete();
        Task::create([
            'task' => 'Complete test',
            'category_id' => 1,
            'status' => 'done',
        ]);
        Task::create([
            'task' => 'Learn Laravel',
            'category_id' => 2,
            'status' => 'processing',
        ]);
        Task::create([
            'task' => 'Rule the world',
            'category_id' => 3,
            'status' => 'todo',
        ]);

    }
}
