<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Task;
use Illuminate\Http\Request;

/**
 * Class TaskController
 * @package App\Http\Controllers
 */
class TaskController extends Controller
{
    public function index(Request $request, Task $taskModel, Category $categoryModel, $category_id = null)
    {
        return view('task.index', [
            'tasks' => $taskModel->search($category_id),
            'categories' => $categoryModel->search(),
            'totalTasks' => $categoryModel->totalTaskCount(),
            'activeCategory' => empty($category_id) ? 0 : $category_id,
            'layout' => $request->ajax() ? 'ajax' : 'app',
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'integer',
            'task' => 'required|min:3|max:255',
            'category_id' => 'required|integer',
            'status' => 'required|in:todo,processing,done',
        ]);

        if ($request->id == 0) {
            $model = new Task;
        } else {
            $model = Task::find($request->id);
        }
        $model->task = $request->task;
        $model->category_id = $request->category_id;
        $model->status = $request->status;
        $model->save();
        exit(json_encode(['success' => true]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Task::find($id);
        $model->delete();
        exit(json_encode(['success' => true]));
    }
}