<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:3|max:255',
        ]);

        $model = new Category();
        $model->title = $request->title;
        $model->save();
        exit(json_encode(['success' => true]));
    }
}
