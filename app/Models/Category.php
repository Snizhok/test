<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App\Models
 */
class Category extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    /**
     * @return mixed
     */
    public function taskCount()
    {
        return DB::table('tasks')
            ->where('category_id', $this->id)
            ->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        return self::orderBy('created_at', 'asc')->get();
    }

    public function totalTaskCount()
    {
        return DB::table('tasks')->count();
    }

}
