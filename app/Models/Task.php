<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['task', 'category_id', 'status'];
    protected $statusLabels = [
        'todo' => 'danger',
        'processing' => 'primary',
        'done' => 'default',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function search($category_id = null)
    {
        $result = self::orderBy('created_at', 'asc');
        if (!empty($category_id)) {
            $result->where('category_id', $category_id);
        }

        return $result->get();
    }

    public function getLabel()
    {
        return $this->statusLabels[$this->status];
    }
}
