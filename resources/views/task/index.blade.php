@extends('layouts.'.$layout)

@section('content')

    <div class="ajax-container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading lead clearfix">
                        Categories
                        <button type="button" class="btn btn-success pull-right create-category" data-toggle="modal"
                                data-target="#create_category_modal">
                            Create New Category
                        </button>
                    </div>
                    <div class="panel-body list-group">

                        <a href="{{ url('/') }}" class="list-group-item @if ($activeCategory == 0) active @endif">
                            <span class="badge">{{ $totalTasks }}</span>
                            All
                        </a>
                        @foreach ($categories as $category)
                            <a href="{{ url('/',['category_id' => $category->id]) }}"
                               class="list-group-item @if ($activeCategory == $category->id) active @endif">
                                <span class="badge">{{ $category->taskCount() }}</span>
                                {{ $category->title }}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading lead clearfix">
                        Tasks
                        <button type="button" class="btn btn-success pull-right create-task" data-toggle="modal"
                                data-target="#create_task_modal">
                            Create New Task
                        </button>
                    </div>
                    <div class="panel-body">
                        <ul class="todo-list ui-sortable">
                            @foreach ($tasks as $task)
                                <li class="{{ $task->status }}">
                                    <span class="text">{{ $task->task }}</span>
                                    <small class="label label-{{ $task->getLabel() }}">
                                        {{ !empty($task->category) ? $task->category->title : 'None' }}
                                    </small>
                                    <div class="tools">
                                        <button class="update-task" data-key="{{ $task->id }}"
                                                data-category="{{ $task->category_id }}"
                                                data-title="{{ $task->task }}" data-status="{{ $task->status }}"
                                                data-toggle="modal" data-target="#create_task_modal">
                                            <i class="glyphicon glyphicon glyphicon-pencil"></i>
                                        </button>
                                        {!!  Form::open([
                                            'route' => ['task.destroy','id'=>$task->id],
                                            'method' => 'POST',
                                            'id' => 'delete-form',
                                        ]) !!}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit"><i class="glyphicon glyphicon-remove-circle"></i></button>
                                        {!! Form::close() !!}
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        @include('task._task_modal')

    </div>

    @include('task._category_modal')

@stop