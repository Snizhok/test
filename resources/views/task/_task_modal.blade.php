<!-- TASK MODAL START -->
<div id="create_task_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!!  Form::open([
                'route' => 'task.store',
                'method' => 'POST',
                'id' => 'task-form',
            ]) !!}
            <input type="hidden" name="id" value="0">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" data-update-task="Update Task" data-create-task="Create New Task">Create New Task</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger hidden">
                    <p><strong>Errors:</strong></p>
                    <ul></ul>
                </div>
                <div class="form-group">
                    <label>Task</label>
                    <input type="text" name="task" class="form-control" placeholder="Task">
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <select name="category_id" class="form-control">
                        <option value="0">None</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control">
                        <option value="todo">To Do</option>
                        <option value="processing">In Process</option>
                        <option value="done">Done</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
                <button type="submit" class="btn btn-primary">
                    Save changes
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- TASK MODAL END -->