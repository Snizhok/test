$(document).ready(function () {

    $(document).on('click', '.create-category', function (e) {
        $('#category-form [name="title"]').val('');
        $('.alert-danger').addClass('hidden');
    });

    $(document).on('click', '.update-task, .create-task', function (e) {
        var className = 'create-task';
        $('.alert-danger').addClass('hidden');
        $('#task-form [name="id"]').val(0);
        $('#task-form [name="task"]').val('');
        $('#task-form [name="category_id"] option').prop('selected', false);
        $('#task-form [name="status"] option').prop('selected', false);
        if ($(this).hasClass('update-task')) {
            className = 'update-task';
            $('#task-form .create-title').addClass('hidden');
            $('#task-form .update-title').removeClass('hidden');
            $('#task-form [name="id"]').val($(this).data('key'));
            $('#task-form [name="task"]').val($(this).data('title'));
            $('#task-form [name="category_id"] option[value="' + $(this).data('category') + '"]').prop('selected', true);
            $('#task-form [name="status"] option[value="' + $(this).data('status') + '"]').prop('selected', true);
        }
        $('#task-form .modal-title').text($('#task-form .modal-title').data(className));
    });

    function updateContent() {
        setTimeout(function () {
            $.get(location.href, {}, function (response) {
                $('.ajax-container').replaceWith($('<div>' + response + '</div>').find('.ajax-container'));
            });
        }, 100);
    }

    $(document).on('submit', '#task-form, #category-form', function (e) {
        e.preventDefault();
        var form = $(this);
        $('.alert-danger').addClass('hidden');
        $.post(form.attr('action'), form.serialize(), function (response) {
            if (response.success) {
                $('#create_task_modal, #create_category_modal').modal('hide');
                updateContent();
            }
        }, 'json').error(function (response) {
            form.find('.alert-danger ul li').remove();
            $.each(response.responseJSON, function (index, element) {
                form.find('.alert-danger ul').append('<li>' + element[0] + '</li>');
            });
            $('.alert-danger').removeClass('hidden');
        });
    });

    $(document).on('submit', '#delete-form', function (e) {
        e.preventDefault();
        var form = $(this);
        if (confirm('Are you sure you want to delete this task?')) {
            $.post(form.attr('action'), form.serialize(), function (response) {
                if (response.success) {
                    updateContent();
                }
            }, 'json').error(function (response) {
                alert('An error occurred while deleting!');
            });
        }
    });

});
